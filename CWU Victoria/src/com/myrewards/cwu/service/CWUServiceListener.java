package com.myrewards.cwu.service;

public interface CWUServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
