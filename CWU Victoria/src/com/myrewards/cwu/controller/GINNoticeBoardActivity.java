package com.myrewards.cwu.controller;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.cwu.model.NoticeBoard;
import com.myrewards.cwu.service.CWUServiceListener;
import com.myrewards.cwu.service.GrabItNowService;
import com.myrewards.cwu.utils.ApplicationConstants;
import com.myrewards.cwu.utils.Utility;

public class GINNoticeBoardActivity extends BaseActivity implements
		CWUServiceListener {
	List<NoticeBoard> noticeBoardProductsList;
	NoticeBoardAdapter mAdapter;
	LayoutInflater inflater;
	public static int count, count1 = 0, count2 = 0, count3 = 0;
	View loading;
	ListView hotOffersListView;
	public static boolean noticecount = false;
	String catID = null;
	String location = null;
	String keyword = null;

	Boolean abc;

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list_gin);
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		setHeaderTitle(getResources().getString(R.string.my_notice_board));
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		hotOffersListView = (ListView) findViewById(R.id.resultsListViewID);
		noticeBoardProductsList = new ArrayList<NoticeBoard>();
		hotOffersListView.setOnItemClickListener(this);
		
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			GrabItNowService.getGrabItNowService().sendNoticeBoardRequest(this);
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
			(ViewGroup) findViewById(R.id.custom_toast_layout_id));
						 
			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			GINNoticeBoardActivity.this.finish();
		}
	}

	public class NoticeBoardAdapter extends BaseAdapter {

		public NoticeBoardAdapter(GINNoticeBoardActivity noticeBoardActivity) {

		}

		@Override
		public int getCount() {
			return noticeBoardProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			if (resultsListRow == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				resultsListRow = (View) inflater.inflate(
						R.layout.results_list_item, null, false);
			}
			LinearLayout rowLL = (LinearLayout) resultsListRow.findViewById(R.id.resultListItemLLID);
			LinearLayout rowLL2 = (LinearLayout)resultsListRow.findViewById(R.id.resultListItemHariLLID);
			rowLL2.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER);
			rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 9.4);
			TextView productNameTV = (TextView) resultsListRow.findViewById(R.id.productTVID);
			productNameTV.setTypeface(Utility.font_bold);
			productNameTV.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER);
			TextView highlightTV = (TextView) resultsListRow.findViewById(R.id.offerTVID);
			highlightTV.setVisibility(View.GONE);
			highlightTV.setTypeface(Utility.font_reg);
			productNameTV.setText(""+noticeBoardProductsList.get(pos).getSubject());
			if (pos%2==0) {
				productNameTV.setTextColor(Color.parseColor("#5D717C"));
			}
			else if (pos%2==1) {
				productNameTV.setTextColor(Color.parseColor("#A6C0CE"));
			}
			switch (pos % 3) {
			case 0:
				rowLL.setBackgroundResource(R.color.result_color_one);
				break;
			case 1:
				rowLL.setBackgroundResource(R.color.result_color_two);
				break;
			case 2:
				rowLL.setBackgroundResource(R.color.result_color_three);
				break;
			case 3:
				rowLL.setBackgroundResource(R.color.result_color_four);
				break;
			}
			return resultsListRow;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		noticecount = true;
		if (pos == 0) {
			if (count1 == 0)
				count1++;
		} else if (pos == 1) {
			if (count2 == 0)
				count2++;
		} else {
			if (count3 == 0)
				count3++;
		}

		if (menuListView.getVisibility() == ListView.GONE) {
			Intent detailsIntent = new Intent(GINNoticeBoardActivity.this, GINNoticeBoardDetailsActivity.class);
			detailsIntent.putExtra(ApplicationConstants.NOTICE_ID_KEY_GIN,noticeBoardProductsList.get(pos).getId());
			detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, pos % 4);
			detailsIntent.putExtra(ApplicationConstants.NOTICE_NAME_KEY_GIN, noticeBoardProductsList.get(pos).getSubject());
			detailsIntent.putExtra(ApplicationConstants.NOTICE_DETAILS_KEY_GIN,	noticeBoardProductsList.get(pos).getDetails());
			startActivity(detailsIntent);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response instanceof String) {
					Utility.showMessage(this, response.toString());
				} else {
					if (eventType == 12) {
						noticeBoardProductsList = (ArrayList<NoticeBoard>) response;
						mAdapter = new NoticeBoardAdapter(this);
						hotOffersListView.setAdapter(mAdapter);
						loading.setVisibility(View.GONE);
					}
				}
			}			
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Server busy. Please wait or Try again", Toast.LENGTH_LONG).show();
			e.getStackTrace();
			e.printStackTrace();
		}
	}
}
