package com.myrewards.cwu.controller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.myrewards.cwu.controller.PagerAdapter;
import com.myrewards.cwu.model.HelpPageFive;
import com.myrewards.cwu.model.HelpPageFour;
import com.myrewards.cwu.model.HelpPageOne;
import com.myrewards.cwu.model.HelpPageSix;
import com.myrewards.cwu.model.HelpPageThree;
import com.myrewards.cwu.model.HelpPageTwo;

import java.util.List;
import java.util.Vector;


/**
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity hosting the ViewPager  
 * @author mwho
 */
public class HelpPagesActivity extends FragmentActivity{
	/** maintains the pager adapter*/
	private PagerAdapter mPagerAdapter;
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.helpviewpager_layout);
		//initialsie the pager
		this.initialisePaging();
	}
	
	/**
	 * Initialise the fragments to be paged
	 */
	private void initialisePaging() {
		
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, HelpPageOne.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageTwo.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageThree.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFour.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFive.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageSix.class.getName()));
		this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		//
		ViewPager pager = (ViewPager)super.findViewById(R.id.viewpager);
		pager.setAdapter(this.mPagerAdapter);
	}

}
