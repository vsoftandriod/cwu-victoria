package com.myrewards.cwu.timer;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.myrewards.cwu.controller.R;

public class Settings extends PreferenceActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
}
